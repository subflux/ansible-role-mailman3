# Ansible role for Mailman 3 installation

## Introduction

[Mailman](http://www.list.org/) is a mailing-list manager. It is a daemon and a WSGI (Python) application with a PostgreSQL database.

Version 3 introduce major installation and configuration changes over version 2, and this role only handles version 3.x.

This role installs and configure the application and the database, but leaves the web server configuration details to your care.
The migration script needed to switch from version 2 is provided.

This work is based on the Fedora infra role:
  http://infrastructure.fedoraproject.org/infra/ansible/roles/mailman/

## Requirements

PostgreSQL and memcached servers should be installed and running before using this role (not necessarily on the same machine).

## Variables

- **site_owner_email**: email of the system owner
- **site_admins**: list of site administrators usernames
                   if defined, grant priviledges to these usernames and revoke to all others
                   if undefined, let the current priviledges untouched
- **from_email**: sender email of messages sent by Mailman
- **webui_name**: Title of the web UI
- **webui_basedir**: where the web interface files are hosted
- **webui_basedir**: where the Mailman UI is to be installed
- **webui_confdir**: configuration directory of the Mailman UI
- **db_server**: hostname of the database server (if remote)
- **db_name**: Mailman main database name
- **db_user**: Mailman user name for the main database
- **http_vhost**: vhost name to create (defaults to the first web domain)
- **hyperkitty_db_name**: Mailman UI database name
- **hyperkitty_db_user**: Mailman user name for the UI database
- **hyperkitty_admin_db_user**: Mailman administrator user name for the UI database
- **domains**: list of web domains managed by Mailman
               it is a hash of domain -> display name
- **with_postfix**: is Mailman working with Postfix
- **memcached_host**: Memcached IP address (if remote)
- **memcached_port**: Memcached port (if non standard)
- **rest_user**: username for the REST API credentials
- **rest_pw**: password for the REST API credentials
- **auth.{service_name}**: enable social authentication for systems requiring an API account
   each `service_name` (free name) has a `provider` (github, google…), a `display_name` as title in the web UI,
   as well as a `client_id` and `client_secret` to connect to the provider

