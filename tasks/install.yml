---

- name: setup the hyperkitty repo
  copy:
    src: "{{item}}"
    dest: /etc/yum.repos.d/hyperkitty.repo
  with_first_found:
  - hyperkitty.{{ansible_hostname}}.repo
  - hyperkitty.{{ansible_distribution}}.repo
  - hyperkitty.repo

- name: install GPG to validate the key
  package:
    name: gnupg
    state: present

- name: add the GPG key
  rpm_key:
    key: https://repos.fedorapeople.org/repos/abompard/abompard.asc
    state: present

# seems some deps should be included into the packages
- name: install needed packages
  package:
    name:
      - python-psycopg2
      - python34-psycopg2
      - hyperkitty
      - hyperkitty-selinux
      - postorius
      - python-pylibmc
      - python-django-haystack-xapian
      - yum-plugin-post-transaction-actions
      - mailman3
      - mailman3-selinux
      - mailman3-hyperkitty
      # django-compressor implicitely requires BeautifulSoup until v1.4
      - python-BeautifulSoup
      # scripts
      - python34-PyYAML
      # mailman soft dep to convert html to plaintext
      - lynx
      # secrets generation
      - apg
    state: present


# install httpd
# needed to get /var/www and config directories
- include_role:
    name: httpd


- user:
    name: "{{ webui_user }}"
    system: true
    comment: "Mailman WebUI User"
    home: "{{ webui_basedir }}"
    shell: /sbin/nologin
  when: webui_user is defined and webui_user



- name: create the hyperkitty configuration directory
  file:
    path: "{{ webui_confdir }}"
    state: directory

- name: install the hyperkitty urls file
  copy:
    src: urls.py
    dest: "{{ webui_confdir }}/urls.py"
    owner: root
    group: root
    mode: 0644
  notify:
    - reload apache

- name: install the hyperkitty wsgi file
  copy:
     src: webui.wsgi
     dest: "{{ webui_confdir }}/webui.wsgi"
     owner: root
     group: root
     mode: 0644
  notify:
    - reload apache

- name: create the fulltext index dir
  file:
    path: "{{ webui_basedir }}/fulltext_index"
    state: directory
    owner: "{{ webui_user }}"
    group: "{{ webui_user }}"
    mode: 0755

- name: create the hyperkitty static files dir
  file:
    path: "{{ webui_basedir }}/static"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: create the hyperkitty templates override dir
  file:
    path: "{{ webui_basedir }}/templates/hyperkitty"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: create the scripts dir
  file:
    path: "{{ webui_basedir }}/bin"
    state: directory
    owner: root
    group: root
    mode: 0755

- name: install the scripts
  copy:
    src: "{{ item }}"
    dest: "{{ webui_basedir }}/bin/{{ item }}"
    owner: root
    group: root
    mode: 0755
  with_items:
  - yamlget
  - pg-give-rights.py
  - post-update.sh
  - import-mm2.py
  - periodic.py

# Sync databases and collect static files on RPM install/upgrade
- name: install the post-transaction trigger
  template:
    src: post-transaction.action.j2
    dest: /etc/yum/post-actions/hyperkitty.action

# Systemd
- name: install the systemd service files
  template:
    src: "{{ item }}.service.j2"
    dest: "/etc/systemd/system/{{ item }}.service"
  with_items:
    - webui-qcluster
    - webui-warm-up-cache
  notify: Reload systemd

- name: create the httpd configuration directory
  file:
    path: "/etc/httpd/conf.d/{{ http_vhost }}.conf.d"
    state: directory

- name: install the django_fedora module
  copy:
    src: django_fedora.py
    dest: "{{ webui_confdir }}/django_fedora.py"
    owner: root
    group: root
    mode: 0644
  notify:
    - reload apache

