---

- set_fact:
    _db_server: "{{ db_server }}"
    _db_server_django: "{{ db_server }}"
- set_fact:
    _db_server: "localhost"
    _db_server_django: ""
  when: "db_server == inventory_hostname"



- name: test possible mailman configuration
  stat:
    path: "/etc/mailman.cfg"
  register: st

- set_fact:
    _need_config: "{{ st.stat.exists is undefined or not st.stat.exists }}"

- block:
    - name: get previously generated database password
      shell: "grep '^url: postgresql://' /etc/mailman.cfg | cut -d: -f4 | cut -d@ -f1"
      register: res1
      changed_when: False
    - set_fact:
        _db_pw: "{{ res1.stdout }}"
  when: not _need_config

- block:
    - name: generate database password
      shell: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res1
      changed_when: False
    - set_fact:
        _db_pw: "{{ res1.stdout }}"
  when: _need_config or _db_pw == ''

- name: generate the mailman configuration
  template:
    src: "{{ item }}"
    dest: /etc/mailman.cfg
    owner: root
    group: mailman
    mode: 0640
  with_first_found:
    - mailman.cfg.{{ ansible_hostname }}.j2
    - mailman.cfg.j2
  notify:
    - restart mailman3



- name: test possible hyperkitty configuration
  stat:
    path: "{{ webui_confdir }}/settings.py"
  register: st

- set_fact:
    _hyperkitty_need_config: "{{ st.stat.exists is undefined or not st.stat.exists }}"

- block:
    - name: get previously generated database password
      shell: "grep -E \"^\\s+'PASSWORD':\" {{ webui_confdir }}/settings.py | cut -d: -f2 | cut -d\\' -f2"
      register: res1
      changed_when: False
    - name: get previously generated secret cookie
      shell: "grep '^SECRET_KEY\\s=' {{ webui_confdir }}/settings.py | cut -d= -f2 | tr -d ' \"'\\'"
      register: res2
      changed_when: False
    - name: get previously generated archiver key
      shell: "grep '^MAILMAN_ARCHIVER_KEY\\s=' {{ webui_confdir }}/settings.py | cut -d= -f2 | tr -d ' \"'\\'"
      register: res3
      changed_when: False
    - set_fact:
        _hyperkitty_db_pw: "{{ res1.stdout }}"
        _hyperkitty_cookie_key: "{{ res2.stdout }}"
        _hyperkitty_archiver_key: "{{ res3.stdout }}"
  when: not _hyperkitty_need_config

- block:
    - name: generate database password
      shell: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res1
      changed_when: False
    - name: generate secret cookie
      shell: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res2
      changed_when: False
    - name: generate archiver key
      shell: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res3
      changed_when: False
    - set_fact:
        _hyperkitty_db_pw: "{{ res1.stdout }}"
        _hyperkitty_cookie_key: "{{ res2.stdout }}"
        _hyperkitty_archiver_key: "{{ res3.stdout }}"
  when: _hyperkitty_need_config or _hyperkitty_db_pw is undefined or _hyperkitty_db_pw == '' or _hyperkitty_cookie_key == '' or _hyperkitty_archiver_key == ''

- name: generate the hyperkitty configuration
  template:
    src: hyperkitty_settings.py.j2
    dest: "{{ webui_confdir }}/settings.py"
    owner: root
    group: "{{ webui_user }}"
    mode: 0640
  notify:
    - reload apache



- name: test possible hyperkitty admin configuration
  stat:
    path: "{{ webui_confdir }}/settings_admin.py"
  register: st

- set_fact:
    _hyperkitty_admin_need_config: "{{ st.stat.exists is undefined or not st.stat.exists }}"

- block:
    - name: get previously generated database password
      shell: "grep -E \"^\\s+'PASSWORD':\" {{ webui_confdir }}/settings_admin.py | cut -d: -f2 | cut -d\\' -f2"
      register: res1
      changed_when: False
    - set_fact:
        _hyperkitty_admin_db_pw: "{{ res1.stdout }}"
  when: not _hyperkitty_admin_need_config

- block:
    - name: generate database password
      shell: "apg -m 16 -x 32 -n 1 -M ncl"
      register: res1
      changed_when: False
    - set_fact:
        _hyperkitty_admin_db_pw: "{{ res1.stdout }}"
  when: _hyperkitty_admin_need_config or _hyperkitty_admin_db_pw is undefined or _hyperkitty_admin_db_pw == ''

- name: generate the hyperkitty configuration
  template:
    src: hyperkitty_settings_admin.py.j2
    dest: "{{ webui_confdir }}/settings_admin.py"
    owner: root
    group: root
    mode: 0600

# Plug HyperKitty into Mailman
- name: generate the mailman-hyperkitty conffile
  template:
    src: mailman-hyperkitty.cfg
    dest: "{{ webui_confdir }}/mailman-hyperkitty.cfg"
    owner: root
    group: mailman
    mode: 0640
  notify:
    - restart mailman3

# To avoid confusion with "{{ webui_confdir }}/mailman-hyperkitty.cfg"
# Don't create a symlink because ansible won't convert the regular file coming
# from the RPM to a symlink.
- name: remove the hyperkitty conffile in the mailman directory to avoid confusion
  file:
    path: /etc/mailman3.d/hyperkitty.cfg
    state: absent

- name: install the migration conffile
  template:
    src: mailman-migration.conf.j2
    dest: /etc/mailman-migration.conf
    owner: root
    group: root
    mode: 0644

- name: install the hyperkitty/postorius dummy httpd conf file
  template:
    src: apache-dummy.conf.j2
    dest: "/etc/httpd/conf.d/{{ item }}.conf"
  with_items:
    - hyperkitty
    - postorius
  notify:
    - reload apache

- name: install the httpd vhost conffile
  template:
    src: apache.conf.j2
    dest: "/etc/httpd/conf.d/{{ http_vhost }}.conf.d/mailman_hyperkitty.conf"
    owner: root
    group: root
    mode: 0644

# contains social auth accounts
- name: copy the initial user fixture
  template:
    src: initial-data.json.j2
    dest: "{{ webui_confdir }}/initial-data.json"
    owner: root
    group: root
    mode: 0640

